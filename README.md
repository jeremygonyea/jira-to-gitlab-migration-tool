# Atlassian Migration Toolkit

## Purpose
As Atlassian is effectively shutting down the on-prem server installs of their various products, $WORK needed to export their data to other services.  This repo provides shims, scripts, etc. to ease the migration of the data in the Atlassian suite.

## Overview
Currently, there are tools to:

* [Migrate JIRA Issues](docs/migrate_jira_to_gitlab.md) to a gitlab instance
* [Convert Bitbucket](docs/convert_bitbucket_repos.md) numbered repository directories to "regular" git namespaced repository directories