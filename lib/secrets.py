'''
Scans for predefined secrets to avoid data leaks.

Regex definitions in `gitleaks.yaml` converted from the Gitleaks project.
'''

import re
import yaml
from colors import Colors

regexes = {}
with open('gitleaks.yaml', 'r', encoding='UTF-8') as file:
    regexes = yaml.safe_load(file)


def scan(fieldname: str, data: str, prompt = False) -> str:
    '''
    Scans for secrets

    Returns:
      "Clean" if no secret is round
      Rule hit if secret found (will also include metadata if found inside a comment)
    '''

    # Don't bother checking bool fields for secrets.
    if isinstance(data, bool):
        return "Clean"

    if isinstance(data, int) or isinstance(data, str):
        for rule in regexes['rules']:
            result = re.search(rule['regex'], str(data))
            if result:
                # Make sure regex hit results aren't on the allow list.
                if not check_allow_regex(result.group()):
                    # Prompt for manual override.
                    if not prompt:
                        return rule['id']
                    if prompt_for_action(fieldname, rule, data, result) is False:
                        return rule['id']
        return "Clean"

    # need to recursively check the comments.
    if fieldname == "comments":
        key = 0
        for comment in data:
            # comment-[nth comment on issue]-[jira comment id]-[author]
            new_comment_fieldname = 'comment-' + str(key) + "-" + str(comment.id) + "-" + comment.updateAuthor.name
            result = scan(new_comment_fieldname, comment.body, prompt)
            if result != "Clean":
                return new_comment_fieldname + " | " + result
            key = key + 1
        return "Clean"


def check_allow_regex(data):
    ''' Double-checks regex rule hit against allow list. '''
    for rule in regexes['allowlist']['regexes']:
        result = re.search(rule, data)
        if result:
            return True
    return False

def prompt_for_action(fieldname, rule, data, result):
    ''' Allow for manual override of possible secret discovery '''
    print(Colors.bg.red, "", Colors.fg.black,"\n\n\n************" + rule['description'] + " Secret Found in "+ fieldname + " ************\n")
    print(Colors.bg.black, "", Colors.fg.red, "Rule: " + rule['regex'])
    print(Colors.bg.black, "", Colors.fg.red, "Result: " + result.group())
    print(Colors.reset)
    accept = ''
    while accept not in ['y', 'Y', 'n', 'N']:
        accept = input("Should we accept this as a false positive? Use 'd' to show the full data field (d/y/n)")
        if accept in ('Y', 'y'):
            return True
        if accept in ('d', 'D'):
            print("Data containing secret:\n")
            print(Colors.bg.blue, "", Colors.fg.lightgrey, data)
            print(Colors.bg.black, "", Colors.fg.red, "Rule: " + rule['regex'])
            print(Colors.bg.black, "", Colors.fg.red, "Result: " + result.group())
            print(Colors.reset)
    return False
