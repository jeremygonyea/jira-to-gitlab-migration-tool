'''
Migrates JIRA issue data to Gitlab

Migrates the following JIRA data to Gitlab:
    * JIRA Issue -> Gitlab Issue
      * Gitlab issues will match JIRA IDs
      * Issues will be assigned based on users.json
    * JIRA issue comments -> Gitlab issue notes
    * JIRA Issue file attachments will be uploaded to the project and linked in a Gitlab issue note.


'''

import json
import re
import logging
import gitlab
from gitlab.exceptions import GitlabGetError
from gitlab.exceptions import GitlabUpdateError
from jira import JIRA, JIRAError
from dotenv import dotenv_values
import progressbar
import urllib3

import lib.assignees as assign
from lib.colors import Colors
import lib.secrets as scanner

# Setup progressbars and logging
progressbar.streams.wrap_stderr()
logging.basicConfig()

# Disable ssl warning for internally hosted sites.
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Feature Flags:

# Enables secrets scanner on data.
PROCESS_SECRETS = True

# Prompt for action if a secret is found.  If set to False, then the issue will be skipped.
PROMPT_SECRETS = False

# Ensure that all issues exist before enabling PROCESS_LINKS.
PROCESS_LINKS = True

# Generates gitlab notes from JIRA comments.
PROCESS_COMMENTS = True

# Uploads attachment files.  PROCESS_COMMENTS also needs to be True.
PROCESS_ATTACHMENTS = True

# Load .env vars.
config = dotenv_values(".env")

def lookup_gl_project_id(project_name):
    """ Load projects data from json file. """
    if project_name in jira_projects:
        return jira_projects[project_name]['gitlabProjectID']
    else:
        raise Exception("Project " + project_name + " not found in projects.json.")

def generate_deleted_payload(iid):
    """ Creates an empty issue payload """
    jira_id = iid.split("-")
    payload = {
        'title': 'DELETED JIRA TICKET',
        'description': 'Imported from JIRA\n\nDeleted ticket.    No data',
        'confidential': True,
        'created_at': '2000-01-01T00:00:00.000-0500',
        'updated_at': '2000-01-01T00:00:00.000-0500',
        'state_event': 'close',
        'labels': '',
        'project_id': lookup_gl_project_id(jira_id[0]),
        'iid': jira_id[1],
        'secrets_scan_result': "Clean",
        'assignee': None,
        'assignee_id': assign.get_gl_id_by_name(None),
        'key': iid
    }

    return payload

def generate_moved_payload(old, new):
    """ Creates an empty issue payload """
    old_id = old.split("-")
    payload = {
        'title': 'Moved JIRA TICKET',
        'description': 'Imported from JIRA\n\nMoved ticket from ' + old + ' => ' + new ,
        'confidential': True,
        'created_at': '2000-01-01T00:00:00.000-0500',
        'updated_at': '2000-01-01T00:00:00.000-0500',
        'state_event': 'close',
        'labels': '',
        'project_id': lookup_gl_project_id(old_id[0]),
        'iid': old_id[1],
        'secrets_scan_result': "Clean",
        'assignee': None,
        'assignee_id': assign.get_gl_id_by_name(None),
        'key': old
    }

    return payload

def get_jira_issue(key: str):
    """ Retrieves JIRA issue data """
    try:
        issue = jira_connection.issue(key)
    except JIRAError as error:
        # Check for deleted issue.
        if error.status_code == 404:
            issue = None
    return issue

def convert_jira_issue(issue):
    """ Prepares data payload to send to Gitlab """
    payload = {}
    jira_id = issue.key.split("-")
    closed_status = jira_projects[jira_id[0]]['closed']
    payload['key'] = issue.key
    payload["title"] = issue.fields.summary
    if issue.fields.description:
        payload["description"] = "Imported from JIRA\n\n" + issue.fields.description
    else:
        payload["description"] = "Imported from JIRA\n\nNo description"
    payload["confidential"] = True
    payload["created_at"] = issue.fields.created
    payload["created_by"] = assign.get_gl_id_by_name(issue.fields.creator)
    payload["assignee_id"] = assign.get_gl_id_by_name(issue.fields.assignee)
    payload["assignee"] = issue.fields.assignee

    if issue.fields.status.name in closed_status:
        payload["state_event"] = "close"
        payload["labels"] = ""
    else:
        payload["state_event"] = "reopen"
        payload["labels"] = issue.fields.status.name

    payload["iid"] = jira_id[1]
    payload["project_id"] = lookup_gl_project(jira_id[0])

    payload["updated_at"] = issue.fields.updated

    if PROCESS_LINKS:
        payload["links"] = issue.fields.issuelinks
    if PROCESS_COMMENTS:
        payload["comments"] = issue.fields.comment.comments
    if PROCESS_ATTACHMENTS:
        payload["attachments"] = issue.fields.attachment
    if PROCESS_SECRETS:
        print("|- Scanning for secrets data")
        payload['secrets_scan_result'] = scan_payload(payload)

    return payload

def upsert_gitlab_issue(payload: dict) -> gitlab.v4.objects.issues.ProjectIssue:
    """ Creates/ Updates Gitlab issue """

    # Find appropriate PAT to use to sign into GitLab.
    token = assign.get_gl_pat_by_name(payload["assignee"])

    # Sign into Gitlab
    project_gl = gitlab.Gitlab(url=config["gitlabURL"], private_token=token)
    project = project_gl.projects.get(payload['project_id'], retry_transient_errors=True)
    try:
        # Check if GitLab issue exists
        issue = project.issues.get(payload["iid"], retry_transient_errors=True)

    except GitlabGetError as ex:
        if ex.response_code == 404:
            print(r" \_ Create new Gitlab issue")

            ## Create issue if it doesn't exist.
            issue = project.issues.create({
                'title': payload['title'],
                'description': payload['description'],
                'iid': payload['iid'],
                'created_at': payload['created_at']
            }, retry_transient_errors=True)
        else:
            print(Colors.bg.red, "", Colors.fg.black,"  |- Couldn't create issue (" + ex.response_code + ")")
            print(Colors.reset)
            log_error(payload['key'], ex.response_code + " | " + ex.response_body )
            return ex.response_code

    issue.title = payload['title']
    issue.description = payload['description']
    issue.confidential = payload['confidential']
    issue.created_at = payload['created_at']
    issue.assignee_id = payload['assignee_id']
    issue.state_event = payload['state_event']
    issue.labels = payload['labels']
    issue.updated_at = payload["updated_at"]


    # Update issue
    try:
        issue.save()
        print("|- Updated Gitlab " + payload['key'] + " metadata")
    except GitlabUpdateError as ex:
        print("  |- Couldn't update issue (" + str(ex.response_code) + ")")
        return str(ex.response_code) + " | " + str(ex.response_body)
    except Exception as ex:
        print("  |- Couldn't update issue. Unknown problem (" + str(ex.__class__.__name__) + ")")
        log_error(payload['key'], str(ex.__class__.__name__))
        return str(ex.__class__.__name__)

    return issue

def lookup_gl_project(project_name):
    """ Load projects data from json file. """

    if project_name in jira_projects:
        return jira_projects[project_name]['gitlabProjectID']
    else:
        raise Exception("Project " + project_name + " not found in projects.json.")

def convert_user_mentions(data: str) -> str:
    ''' Replaces JIRA username callouts with gitlab ones '''
    users = assign.get_all_users()
    result = data
    for user in users:
        pattern = '\[~' + user + '\]'
        replacement = "@" + users[user]['gitlabUser']
        result = re.sub(pattern, replacement, result)

    return result

def process_comments(issue: gitlab.v4.objects.issues.ProjectIssue, payload: dict):
    ''' Creates/ Updates comments on a Gitlab issue '''
    print("|- Processing comments on issue")
    print(r" \_ Migrating JIRA comments")

    gitlab_project_id = payload['project_id']
    # Clear old issue notes.
    # Gitlab has no way of setting an issue note id like we can on an issue, so we have to just wipe out all existing notes and start fresh.
    for note in issue.notes.list(all=True):
        if not note.system:
            note.delete(retry_transient_errors=True)

    # Create new issue notes.
    # Gitlab notes has no way of setting the author field.  We'll be pulling individual's gitlab PAT's to set the note authors appropriately.
    count = len(payload["comments"])
    print(r"  \_ Creating new comments (" + str(count) + ")")
    widgets = [progressbar.Percentage(), progressbar.Bar()]
    bar_graph = progressbar.ProgressBar(widgets=widgets, max_value=count).start()
    item = 0
    for comment in payload['comments']:
        user_pat = assign.get_gl_pat_by_name(comment.author)
        user_gl = gitlab.Gitlab(url=config["gitlabURL"], private_token=user_pat, retry_transient_errors=True)
        project = user_gl.projects.get(gitlab_project_id)
        issue = project.issues.get(payload["iid"], retry_transient_errors=True)
        author = {
            "id": assign.get_gl_id_by_name(comment.author)
        }
        body_text = "Migrated JIRA comment by " + comment.author.name + ": \n\n" + convert_user_mentions(comment.body)
        note = issue.notes.create({'body': body_text, 'author': author, 'created_at': comment.created})
        note.attributes['author'] = author
        note.save()
        bar_graph.update(item + 1)
        item += 1

def process_attachments(issue: gitlab.v4.objects.issues.ProjectIssue, payload:dict):
    ''' Transfers attachment contents to Gitlab '''

    # Gitlab has no concept of attachments belonging to an issue.  Each attachment needs to be attached to a note.
    # Gitlab notes has no way of setting the author field.  We'll be pulling individual's gitlab PAT's to set the note authors appropriately.

    # Create a comment that will hold our file attachment.

    count = len(payload["attachments"])
    gitlab_project_id = payload['project_id']

    print(r"  \_ Migrating JIRA attachments (" + str(count) + ")")
    widgets = [progressbar.Percentage(), progressbar.Bar()]
    bar_graph = progressbar.ProgressBar(widgets=widgets, max_value=count).start()
    item = 0
    for attachment in payload["attachments"]:
        if attachment.size > 10485760:  # 10Mb filesize limit
            print(Colors.bg.red, "", Colors.fg.black,"!! Could not upload file " + attachment.filename + ".  Attachment too large")
            print(Colors.reset)
            log_error(payload['key'], "Attachment too large: " + attachment.filename)
            continue
        # Get gitlab user and project.
        user_pat = assign.get_gl_pat_by_name(attachment.author)
        user_gl = gitlab.Gitlab(url=config["gitlabURL"], private_token=user_pat, retry_transient_errors=True)
        project = user_gl.projects.get(gitlab_project_id)
        issue = project.issues.get(payload["iid"], retry_transient_errors=True)

        # Create new gitlab note and attach file.
        attachment_author = "\nOriginally uploaded by " + attachment.author.name
        file = project.upload(attachment.filename, filedata=attachment.get())
        issue.notes.create({
            "body": "Migrated Jira attachment [" + attachment.filename + "]{}".format(file['markdown']) + attachment_author,
            "created_at": attachment.created
        })
        bar_graph.update(item + 1)
        item += 1

def process_jira_issue(key: str, process_issue_only = False):
    """ Processes a passed JIRA key """
    if process_issue_only:
        print(Colors.bg.blue, "", Colors.fg.black, "|- Loading " + key + " from JIRA")
    else:
        print(Colors.bg.green, "", Colors.fg.black,"|- Loading " + key + " from JIRA")
    print(Colors.reset)
    jira_issue = get_jira_issue(key)

    # Does JIRA issue exist?
    if jira_issue is None:
        payload = generate_deleted_payload(key)
        print("|- JIRA issue missing. Using blank issue template")

    # Was the JIRA issue moved between projects?
    elif jira_issue.key != key:
        print("|- JIRA Issue was moved from its original project.")
        payload = generate_moved_payload(key, jira_issue.key)

    else:
        payload = convert_jira_issue(jira_issue)

    if "secrets_scan_result" in payload and payload['secrets_scan_result'] != "Clean":
        print(Colors.bg.red, "", Colors.fg.black, " !! " + payload['secrets_scan_result'])
        print(Colors.reset)
        # Save issue id to file for followup.  DO NOT UPLOAD TO GITLAB
        log_error(key, payload['secrets_scan_result'])
        return False

    issue = upsert_gitlab_issue(payload)

    # If flagged as true, only the issue stub will be generated.
    if process_issue_only == True:
        return

    if 'comments' in payload and PROCESS_COMMENTS:
        process_comments(issue, payload)

    if 'attachments' in payload and PROCESS_ATTACHMENTS:
        process_attachments(issue, payload)

    if 'links' in payload and PROCESS_LINKS and PROCESS_COMMENTS:
        upsert_issue_links(issue, payload)

    return True

def load_jira_issues(jira_project_name, jira_start, jira_end):
    """ Loads JIRA project """

    # Iterate through each JIRA issue in project.
    for i in progressbar.progressbar(range(jira_start, jira_end)):
        ### Begin load here
        key = jira_project_name  + "-" + str(i)
        process_jira_issue(key)

def load_jira_projects(path):
    """ Load projects data from json file. """

    with open(path, 'r', encoding='UTF-8') as projects:
        projects = json.load(projects)
    return projects

def log_error(key, reason):
    ''' Saves error to logfile '''
    jira_issue = key.split("-")
    with open('data/logs/' + jira_issue[0] + ".log", 'a', encoding="UTF-8") as file1:
        file1.write(str(jira_issue[1]) + " | " + reason)
        file1.write("\n")

def scan_payload(payload):
    ''' Iterates through payload for secrets '''
    if not PROCESS_SECRETS:
        return "Clean"

    # Iterate through payload fields.
    for fieldname, value in payload.items():
        if fieldname == "comments" and not PROCESS_COMMENTS:
            # Don't parse comments if PROCESS_COMMENTS is turned off.
            continue
        if fieldname in  ["assignee", "attachments", "links"]:
            # Don't scan.
            continue

        scan_result = scanner.scan(fieldname, value, PROMPT_SECRETS)
        if scan_result != "Clean":
            return scan_result + " hit on field '" + fieldname + "'"
    return "Clean"

def upsert_issue_links(inward_issue, payload):
    """ Create inter-issue links on Gitlab """
    for link in payload['links']:

        try:
            getattr(link, 'outwardIssue')
            key = link.outwardIssue.key
        except AttributeError:
            getattr(link, 'inwardIssue')
            key = link.inwardIssue.key

        outward_jira_issue = get_jira_issue(key)


        iid = outward_jira_issue.key.split("-")[1]

        issue_exists = False

        # Find appropriate PAT to use to sign into GitLab.
        token = assign.get_gl_pat_by_name(outward_jira_issue.fields.creator)
        outward_project_id = lookup_gl_project_id(outward_jira_issue.key.split("-")[0])

        # Sign into Gitlab
        project_gl = gitlab.Gitlab(url=config["gitlabURL"], private_token=token)
        project = project_gl.projects.get(outward_project_id, retry_transient_errors=True)

        while not issue_exists:
            try:
                # Check if GitLab issue exists
                project.issues.get(iid, retry_transient_errors=True)
                issue_exists = True
                outward_issue_created = True

            except GitlabGetError as ex:
                if ex.response_code == 404:
                    print(r" \_ Missing GitLab linked issue.  Creating issue stub")
                    outward_issue_created = process_jira_issue(outward_jira_issue.key, True)

            if not outward_issue_created:
                # Skip creating link.
                return

        link_data = {
            'target_project_id': outward_project_id,
            'target_issue_iid': iid
        }
        try:
            inward_issue.links.create(link_data)
        except gitlab.GitlabCreateError as ex:
            if ex.response_code != 409:
                print(Colors.bg.red, "", Colors.fg.black,"  |- Couldn't create issue link (" + str(ex.response_code) + ")")
                print(Colors.reset)
                log_error(payload['key'], "Couldn't create link to " + outward_jira_issue)

if __name__=="__main__":
    jira_projects = load_jira_projects('data/projects.json')

    jira_connection = JIRA(
        config['jiraBaseURL'],
        auth=(config['username'], config['password']),
        options={'verify' : False}
    )

    for jira_project, data in jira_projects.items():
        print("Starting on " + jira_project)
        load_jira_issues(jira_project, data['start'], data['end'])
        print("Finished " + jira_project + "\n")
