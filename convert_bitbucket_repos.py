'''
Converts Atlassian Bitbucket git repositories to regular git repositories.

By design, Atlassian Bitbucket Server stores its repositories within numerically named folders.

This script generates a bash script to rename them based upon their clone url namespaces.
'''

import os
import requests
import urllib3
from dotenv import dotenv_values
import progressbar
from requests.auth import HTTPBasicAuth


# Disable ssl warning for internally hosted sites.
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Load .env vars.
config = dotenv_values(".env")

def get_bitbucket_projects() -> dict:
    """ Retrieves each project """
    bitbucketBaseURL = config["bitbucketBaseURL"]
    projects = {}
    last_page = False
    start = 0
    limit = 50
    
    # Paginate through projects.
    while last_page == False:
        response = requests.get(bitbucketBaseURL, auth=HTTPBasicAuth(config["username"], config["password"]), verify=False, params = {'start': start, 'limit' : limit})
        for project in response.json()['values']:
            projects[project['key']] = project
        last_page = response.json()['isLastPage']
        if "nextPageStart" in response.json():
            start = response.json()['nextPageStart']
    return projects

def get_bitbucket_repos(projects) -> dict:
    """ Retrieves each project's repos """
    repos = {}
    widgets = [' [',
        progressbar.Timer(format= 'elapsed time: %(elapsed)s'),
        '] ',
        progressbar.Bar('*'),' (',
        progressbar.ETA(), ') ',
    ]
    bar = progressbar.ProgressBar(maxval=len(projects), widgets=widgets).start()
    i = 0

    for key, project in projects.items():
        bar.update(i)
        i = i + 1
        bitbucketBaseURL = config["bitbucketBaseURL"] + "/" + key + "/repos"
        start = 0
        limit = 25
        last_page = False

        # Paginate through repos.
        while last_page == False:
            response = requests.get(bitbucketBaseURL, auth=HTTPBasicAuth(config["username"], config["password"]), verify=False, params = {'start': start, 'limit' : limit})
            for repo in response.json()['values']:
                hrefs = repo['links']['clone']
                href_exploded = ""
                for href in hrefs:
                    if href['name'] == 'ssh':
                        href_exploded = href['href'].split('/')
                id = repo['id']

                # Converts git clone url to new directory namespace.
                del href_exploded[0]
                del href_exploded[0]
                del href_exploded[0]
                repos[id] = "/".join(href_exploded)

            last_page = response.json()['isLastPage']
            if "nextPageStart" in response.json():
                start = response.json()['nextPageStart']

    return repos

def generate_rename_script(repos):
    """ Converts repo list into bash script """
    if os.path.exists("data/convert_id_to_namespace.sh"):
        os.remove("data/convert_id_to_namespace.sh")
    script = open("data/convert_id_to_namespace.sh", "a", encoding="utf-8")
    script.write("#! /bin/bash\n")
    for key, repo in repos.items():
        # Create target dir.
        script.write("mkdir -p " + repo + "\n")
        # Move files from Bitbucket named directory to target dir.
        script.write("mv " + str(key) + "/{.,}* " + repo + "\n")
        # Removes source dir.
        script.write("rm -rf " + str(key) + "\n")

def main():
    """ Main function """
    # Enumerate projects.
    projects = get_bitbucket_projects()

    # Enumerate repos within each project.
    repos = get_bitbucket_repos(projects)

    # Generate renaming script.
    generate_rename_script(repos)

if __name__=="__main__":
    main()